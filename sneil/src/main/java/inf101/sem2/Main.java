package inf101.sem2;

import java.awt.Color;

import javax.swing.JFrame;

import inf101.sem2.game.controller.Controller;
import inf101.sem2.game.model.structure.GameModel;
import inf101.sem2.game.model.visuals.GameVisual;

/** The Main class is where the program runs from when opened */
public class Main {
    
    public static final String WINDOW_TITLE = "Happy Sneil";


    public static void main(String[] args) {
        GameModel model = new GameModel();
        GameVisual visuals = new GameVisual(model);
        new Controller(model, visuals);

  
    //** Jframe below sets up the window frame for the game. */
    JFrame jframe = new JFrame(WINDOW_TITLE);

    jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    jframe.setContentPane(visuals);
    
    jframe.setBackground(new Color(45,122,56,255));
    jframe.setSize(950, 500);
    jframe.setLocationRelativeTo(null);
    jframe.setResizable(false);
    jframe.setVisible(true);

    }
}
