package inf101.sem2.game.model.structure;

import java.awt.Color;

/** The tile is where color, number and symbol is added to make a character or board with.  */
public class Tile {

    public Color color;
    public char symbol;
    public int number;

    /**
     * The constructor of Tile takes in color, symbol and number to be able modify the result.
     * @param color
     * @param symbol
     * @param number
     */
    public Tile(Color color, char symbol, int number) {
    this.number = number;
    this.color = color;
    this.symbol = symbol;
    }

}
