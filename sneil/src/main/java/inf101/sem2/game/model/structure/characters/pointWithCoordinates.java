package inf101.sem2.game.model.structure.characters;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import inf101.sem2.grid.CoordinatesThings;
import inf101.sem2.grid.Coordinates;
import inf101.sem2.game.model.structure.Tile;

/** The class of the Iterable CoordinateThings creates characters with coordinates. */
public class pointWithCoordinates implements Iterable<CoordinatesThings<Tile>> {

    Character character;
    Coordinates coordinate;
    List<CharacterWithCoordinates> charList = new ArrayList<>();

    Tile tile;
    int subRow;
    int subCol;

    /**
     * The instructor of the class takes in an optional Character(made from a Tile and a 2dBoolean) and a Coordinate to iterate it through the board.
     * @param character
     * @param coordinate
     */
    public pointWithCoordinates(Character character, Coordinates coordinate) {
        this.character = character;
        this.coordinate = coordinate;
    }

    @Override
    public Iterator<CoordinatesThings<Tile>> iterator() {
        List<CoordinatesThings<Tile>> coordinateCharacter = new ArrayList<CoordinatesThings<Tile>>();
        for (int height = 0; height < character.getHeight(); height++) {
            for (int width = 0; width < character.getWidth(); width++) {
                Coordinates coordinates = new Coordinates(height + coordinate.row, width + coordinate.col);
                CoordinatesThings<Tile> coordinateThing = new CoordinatesThings<Tile>(coordinates, character.getTile());

                if (character.getShape()[height][width] == true) {
                coordinateCharacter.add(coordinateThing);
                }
            
            }
        }
        return coordinateCharacter.iterator();
    }
    

    /**
     * CharacterWithCoord takes the coordinates given as input and connects the character with the coordinates.
     * @param subRow
     * @param subCol
     * @return
     */
    public CharacterWithCoordinates CharacterWithCoord(int subRow, int subCol) {
        CharacterWithCoordinates charCoordinates = new CharacterWithCoordinates(this.character, new Coordinates(this.coordinate.row + subRow, this.coordinate.col + subCol));
        this.subRow = subRow;
        this.subCol = subCol;
        return charCoordinates;
    }

    /**@return the coordinates*/
    public Coordinates getCoordinate() {
        return this.coordinate;
    }


    
}
