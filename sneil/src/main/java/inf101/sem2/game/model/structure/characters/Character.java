package inf101.sem2.game.model.structure.characters;

import java.awt.Color;
import java.util.List;

import inf101.sem2.game.model.structure.Tile;

/** The class creates a character with Tile and a 2dBoolean. */
public class Character {

    Tile tile;
    boolean[][] shape;
    List<Character> newList;
    

    /** The instructor of the class takes in a Tile and a 2dBoolean to create a character.
     * @param tile
     * @param shape */
    
    public Character(Tile tile, boolean[][] shape) {
    this.tile = tile;
    this.shape = shape;
    }


    // Board
    static final Character Board = new Character(
        new Tile(Color.PINK, 'B', 1),
        new boolean[][] {
            { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
            { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
            { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
            { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
            { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
            { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
            { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
            { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
            { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
            { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
            { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
            { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false }
        }
    );

       // Sneil
       static final public Character Sneil = new Character(
        new Tile(Color.YELLOW, 'S', 0),
        new boolean[][] {
            { true, },
        }
    );

      // Flower
      static final public Character Flower = new Character(
        new Tile(Color.PINK, 'F', 0),
        new boolean[][] {
            { true }
            
        }
        
    );

    /** @return the shape of the 2dboolean.*/
    boolean[][] getShape() {
        return shape;
        }

    /**  @return The shape of the 2dboolean. */
    int getHeight() {
    return shape.length;
    }

    /** @return The width of the 2dboolean. */
    int getWidth() {
        return shape[0].length;
        }

    /** @return the Tile. */
    Tile getTile() {
        return tile;
        }


}
