package inf101.sem2.game.model.visuals;
import inf101.sem2.game.GameStates;
import inf101.sem2.game.model.structure.Board;
import inf101.sem2.game.model.structure.GameModel;
import inf101.sem2.game.model.structure.Tile;
import inf101.sem2.grid.CoordinatesThings;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JComponent;

import java.awt.Font;

//** This class draws and visualizes the game that is seen with jframe */
public class GameVisual extends JComponent {

    IGameVisual iVisual;
    Board board;
    GameModel model;

    {
        // This code (between curly braces) is executed when an object is
        // created (before the call to the constructor, if one exists). 
        
        // The call to setFocusable enables the panel to receive events from
        // the user, such as key-presses and mouse movements.
        this.setFocusable(true);
    }


    public GameVisual(IGameVisual iVisual) {
        this.iVisual = iVisual;
    }
    

    @Override
    public void paint(Graphics canvas) {
        super.paint(canvas);
        this.drawGame(canvas, 0, 0, this.getWidth(), this.getHeight(), 2);
    }


    /**
     * The method takes in the input from different boards, and visualises them.
     * @param canvas
     * @param coordinateX
     * @param coordinateY
     * @param width
     * @param height
     * @param padding
     */
    private void drawGame(Graphics canvas, int coordinateX, int coordinateY, int width, int height, int padding) {
        drawBoard(canvas, coordinateX+15, coordinateY+15, width-230, height-30, padding);
        drawTilesOnBoard(canvas, coordinateX+15, coordinateY+15, width-230, height-30, padding, this.iVisual.characterOnBoard());
        drawTilesOnBoard(canvas, coordinateX+15, coordinateY+15, width-230, height-30, padding, this.iVisual.pointsOnBoard());
        gameOver(canvas);
        gamePause(canvas);
        welcomeScreen(canvas);
        activeGame(canvas);
    }

    /** Draws the board. */
    private void drawBoard(Graphics canvas, int boardX, int boardY, int boardWidth, int boardHeight, int padding) {
        for (CoordinatesThings<Tile> coordinateItem : this.iVisual.tilesOnBoard()) {
            int row = coordinateItem.coordinate.row;
            int column = coordinateItem.coordinate.col;
            Tile tile = coordinateItem.thing;
            Color color;
            color= new Color(134,240, 148 ,255); 
            if (tile != null) {
                color = tile.color;
            }
            

            int tileX = boardX + column * boardWidth / this.iVisual.getCols();
            int tileY = boardY + row * boardHeight / this.iVisual.getRows();
            int nextTileX = boardX + (column + 1) * boardWidth / this.iVisual.getCols();
            int nextTileY = boardY + (row + 1) * boardHeight / this.iVisual.getRows();
            int tileWidth = nextTileX - tileX;
            int tileHeight = nextTileY - tileY; 

            this.drawPadding(canvas, tileX, tileY, tileWidth, tileHeight, color, padding);  
    }  
}
    /** Adds padding to the tiles. */
    private void drawPadding(Graphics canvas, int coordinateX, int coordinateY, int width, int height, Color color, int padding) {
        canvas.setColor(color);
        canvas.fillRect(coordinateX, coordinateY, width - padding, height - padding);
    }

    /** Draws the tiles(character and flower) of characters on the board. */
    private void drawTilesOnBoard(Graphics canvas, int boardX, int boardY, int boardWidth, int boardHeight, int padding, Iterable<CoordinatesThings<Tile>> iterable) {
        for (CoordinatesThings<Tile> coordinateItem: iterable) {
            int row = coordinateItem.coordinate.row;
            int column = coordinateItem.coordinate.col;
            Tile tile = coordinateItem.thing;
            Color color = Color.PINK;
            if (tile != null) {
                color = tile.color;
            }
            if (iterable == (iVisual.pointsOnBoard())) {
                color= new Color(247,47,60,255);
            }

            int tileX = boardX + column * boardWidth / this.iVisual.getCols();
            int tileY = boardY + row * boardHeight / this.iVisual.getRows();
            int nextTileX = boardX + (column + 1) * boardWidth / this.iVisual.getCols();
            int nextTileY = boardY + (row + 1) * boardHeight / this.iVisual.getRows();
            int tileWidth = nextTileX - tileX;
            int tileHeight = nextTileY - tileY; 

            this.drawPadding(canvas, tileX, tileY, tileWidth, tileHeight, color, padding);
        }
        }



            @Override
            public Dimension getPreferredSize() {
                int newWidth = (40+2)*iVisual.getCols()+2;
                int newHeight = (40+2)*iVisual.getRows()+2;
                return new Dimension(newWidth, newHeight);
            }



            //The methods below creates and visualises gamemodes like the welcomeScreen, gameOverScreen and etc.

        /**
         * 
         * @param canvas
         */
        private void welcomeScreen(Graphics canvas){
            if(iVisual.getGameState()==GameStates.WELCOME_SCREEN){
            canvas.setColor(new Color(149, 224, 144, 255));
            canvas.fillRect(getX(), getY(), getWidth(), getHeight());
            Font font = new Font("Courier New", Font.BOLD, 50);
            canvas.setFont(font);
            canvas.setColor(Color.WHITE);
            GraphicHelperMethods.drawCenteredString(
                    canvas, "HAPPY SNEIL",
                    20, 150, getWidth() - 40, getHeight() - 540);
    
            Font font1 = new Font("SansSerif", Font.BOLD, 20);
            canvas.setFont(font1);
            canvas.setColor(new Color(61, 117, 83 ,255));
            GraphicHelperMethods.drawCenteredString(
                    canvas, "Press space to start game",
                    20, 195, getWidth() - 40, getHeight() - 540);

            Font font2 = new Font("Monospaced", Font.BOLD, 20);
            canvas.setFont(font2);
            canvas.setColor(Color.WHITE);
            GraphicHelperMethods.drawCenteredString(
                    canvas, "Happy Sneil is the story about a snail called Sneil wandering ",
                    20, 300, getWidth() - 40, getHeight() - 540);
            
            Font font3 = new Font("Monospaced", Font.BOLD, 20);
            canvas.setFont(font3);
            canvas.setColor(Color.WHITE);
            GraphicHelperMethods.drawCenteredString(
                        canvas, "the meadows in hope to get himself some tasty flowers to eat.",
                        20, 320, getWidth() - 40, getHeight() - 540);

            Font font4 = new Font("Monospaced", Font.BOLD, 20);
            canvas.setFont(font4);
            canvas.setColor(Color.WHITE);
            GraphicHelperMethods.drawCenteredString(
                    canvas, "Whenever he eats, he gets bigger and slimier. Watch out!",
                    20, 340, getWidth() - 40, getHeight() - 540);
            
            Font font5 = new Font("Monospaced", Font.BOLD, 20);
            canvas.setFont(font5);
            canvas.setColor(Color.WHITE);
            GraphicHelperMethods.drawCenteredString(
                    canvas, " You might want to avoid walking in your own slime,",
                    20, 360, getWidth() - 40, getHeight() - 540);


            Font font6 = new Font("Courier New", Font.BOLD, 20);
            canvas.setFont(font6);
            canvas.setColor(Color.WHITE);
            GraphicHelperMethods.drawCenteredString(
                    canvas, " you might not ever get out again.",
                    20, 380, getWidth() - 40, getHeight() - 540);
                }
        }

        private void gamePause(Graphics canvas){
            if(iVisual.getGameState()==GameStates.PAUSED_GAME){
            canvas.setColor(new Color(0, 0, 0, 150));
            canvas.fillRect(getX(), getY(), getWidth(), getHeight());
            Font font = new Font("Courier New", Font.BOLD, 30);
            canvas.setFont(font);
            canvas.setColor(Color.GREEN);
            GraphicHelperMethods.drawCenteredString(
                    canvas, "PAUSE",
                    20, 220, getWidth() - 40, getHeight() - 540);
    
            Font font1 = new Font("SansSerif", Font.BOLD, 20);
            canvas.setFont(font1);
            canvas.setColor(Color.WHITE);
            GraphicHelperMethods.drawCenteredString(
                    canvas, "Press esc to resume game",
                    20, 260, getWidth() - 40, getHeight() - 540);
                }
        }

        private void gameOver (Graphics canvas){
            if(iVisual.getGameState()==GameStates.GAMEOVER_SCREEN){
            canvas.setColor(new Color(149, 224, 144, 255));
            canvas.fillRect(getX(), getY(), getWidth(), getHeight());
            Font font = new Font("Courier New", Font.BOLD, 50);
            canvas.setFont(font);
            canvas.setColor(new Color(130, 26, 66,255));
            GraphicHelperMethods.drawCenteredString(
            canvas, "Game Over", 
            20, 220, getWidth() - 40, getHeight() - 540);

            Font font1 = new Font("Courier New", Font.BOLD, 20);
            canvas.setFont(font1);
            canvas.setColor(Color.WHITE);
            GraphicHelperMethods.drawCenteredString(
            canvas, "FINAL SCORE = " + iVisual.getScore(), 
            20, 275, getWidth() - 40, getHeight() - 540);

            Font font5 = new Font("Courier New", Font.BOLD, 20);
            canvas.setFont(font5);
            canvas.setColor(Color.WHITE);
            GraphicHelperMethods.drawCenteredString(
            canvas, "Press Space to return to menu ", 
            20, 300, getWidth() - 40, getHeight() - 540);
            }  
            }   

            private void activeGame(Graphics canvas){
                if(iVisual.getGameState()==GameStates.ACTIVE_GAME || iVisual.getGameState()==GameStates.PAUSED_GAME ){
                //canvas.setColor(new Color(149, 224, 144, 255));
                //canvas.fillRect(getX(), getY(), getWidth(), getHeight());
                Font font = new Font("Courier New", Font.BOLD, 30);
                canvas.setFont(font);
                canvas.setColor(Color.WHITE);
                GraphicHelperMethods.drawCenteredString(
                        canvas, "HAPPY SNEIL",
                        380, 80, getWidth() - 40, getHeight() - 540);
        
                Font font1 = new Font("SansSerif", Font.BOLD, 18);
                canvas.setFont(font1);
                canvas.setColor(Color.WHITE);
                GraphicHelperMethods.drawCenteredString(
                        canvas, "Press ESC for pause",
                        380, 450, getWidth() - 40, getHeight() - 540);


                Font font2 = new Font("SansSerif", Font.BOLD, 15);
                canvas.setFont(font2);
                canvas.setColor(new Color(182, 222, 184,255));
                GraphicHelperMethods.drawCenteredString(
                    canvas, "SCORE = " + iVisual.getScore(),
                    380, 120, getWidth() - 40, getHeight() - 540);        
                    }
            }

            
        }

  
    
