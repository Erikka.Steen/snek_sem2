package inf101.sem2.game.model.structure.characters;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import inf101.sem2.grid.CoordinatesThings;
import inf101.sem2.grid.Coordinates;
import inf101.sem2.game.model.structure.Tile;

/** The class of the Iterable CoordinateThings creates characters with coordinates. */
public class CharacterWithCoordinates implements Iterable<CoordinatesThings<Tile>> {

    Character character;
    Coordinates coordinate;
    List<CoordinatesThings<Tile>> characterList = new ArrayList<>();
    boolean grow = false;
    boolean isDead = false;

    Tile tile;
    int row;
    int col;

    /**
     * The instructor of the class takes in an optional Character(made from a Tile and a 2dBoolean) and a Coordinate to iterate it through the board.
     * @param character
     * @param coordinate
     */
    public CharacterWithCoordinates(Character character, Coordinates coordinate) {
        this.character = character;
        this.coordinate = coordinate;
        characterList.add(new CoordinatesThings<Tile>(coordinate = new Coordinates(5,4), (new Tile(Color.WHITE, 'S',1))));
        characterList.add(new CoordinatesThings<Tile>(coordinate = new Coordinates(5,3), (new Tile(Color.WHITE, 'S',1))));
    }


    @Override
    public Iterator<CoordinatesThings<Tile>> iterator() {
        return characterList.iterator();
    }
    
    /** A helping method to make the character grow when it has eaten */
    public void eat(){
        grow = true;
    }


    public void moveCharacter(int deltarow, int deltacol) {
         Coordinates coordinateHead = characterList.get(characterList.size()-1).coordinate;
         Coordinates  cord = new Coordinates(coordinateHead.row + deltarow, coordinateHead.col + deltacol);
         CoordinatesThings<Tile> a = new CoordinatesThings<Tile>(this.coordinate = cord ,(new Tile(Color.WHITE, 'S',1)));

         /** We check if the coordinate of the character touches a coordinate/it's own tail - that if it already exists in its own list, it dies. */
         for (CoordinatesThings<Tile> coordinatesThings : characterList) {    
         if(cord.equals(coordinatesThings.coordinate)){
             isDead = true;
         }
         }
         
         characterList.add(a);
         
         if (grow) {
            grow = false;
         }
         else {
            characterList.remove(0);
        }

   }


    /**@return the coordinates*/
    public Coordinates getCoordinate() {
        return this.characterList.get(characterList.size()-1).coordinate;
    }

    /**
     * If the character touches it's own tail, we return true that it's dead.
     * @return
     */
    public boolean isDead() {
        return isDead;
    }    
}
