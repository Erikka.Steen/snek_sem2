package inf101.sem2.game.model.visuals;
import inf101.sem2.game.model.structure.Tile;
import inf101.sem2.grid.CoordinatesThings;
import inf101.sem2.game.Directions;
import inf101.sem2.game.GameStates;

//** Interface of the frontend  */
public interface IGameVisual {
    
    /** @return the rows */
    int getRows();

    /** @return the cols */
    int getCols();

    /** @return the score*/
    int getScore();

    /**  Iterates the tiles onto the board, making the board visual.
     * @return the tiles on the board. */
    Iterable<CoordinatesThings<Tile>> tilesOnBoard();

    /** Iterates the character onto the board.
     *  @return the characters on the board. */
    Iterable<CoordinatesThings<Tile>> characterOnBoard();

    /** Iterates the flower onto the board.
     *  @return the flower on the board. */
    public Iterable<CoordinatesThings<Tile>> pointsOnBoard();

    /** Counts how many times the character has the same coordinate as the point(flower) and relocates the point(flower) */
    public void makesPoints();

    /** Lets you change the gamestate.
    * @return a new gamestate */
    GameStates setGameState(GameStates newgamestates);

    /** @return the GameState the game is in. */
    GameStates getGameState();

    /** @return the Direction the player is in. */
    Directions getDirection();
}   
