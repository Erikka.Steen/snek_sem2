package inf101.sem2.game.model.structure;

import java.lang.Iterable;
import java.util.Random;

import inf101.sem2.game.Directions;
import inf101.sem2.game.GameStates;
import inf101.sem2.game.controller.IGame;
import inf101.sem2.game.model.structure.characters.Character;
import inf101.sem2.game.model.structure.characters.CharacterWithCoordinates;
import inf101.sem2.game.model.structure.characters.pointWithCoordinates;
import inf101.sem2.game.model.visuals.IGameVisual;
import inf101.sem2.grid.Coordinates;
import inf101.sem2.grid.CoordinatesThings;


/** The class GameModel connects all the pieces together to be able to visualize them. */
public class GameModel implements IGameVisual, IGame {

    private Board board;
    public int flowersEaten;
    Tile tile;
    int rows = 15;
    int cols = 25;
    Coordinates coord = new Coordinates(rows/2,cols/2);
    private Random random = new Random();
    int randomNumberRows = random.nextInt(rows);
    int randomNumberCols = random.nextInt(cols);
    Coordinates randCoord = new Coordinates(randomNumberRows, randomNumberCols);
    
    CharacterWithCoordinates characterHead;
    pointWithCoordinates point;
   
    GameStates gamestate;
    Directions directions;
    
    /** The constructor of the class adds a board, a gamestate, a direction where the character starts, a character(the Sneil) and a point(the flower) */
    public GameModel() { 
    this.flowersEaten = 0;
    this.board = new Board(rows, cols);
    this.gamestate = GameStates.WELCOME_SCREEN;
    this.directions = Directions.NORTH;

    this.characterHead = new CharacterWithCoordinates(Character.Sneil, this.coord);
    this.point = new pointWithCoordinates(Character.Flower, randCoord);

    }


    @Override
    public boolean outOfBounds(CharacterWithCoordinates charaHead) {
        for (CoordinatesThings<Tile> tile : charaHead) {
            if(!board.CheckIfCoordinateIsOnGrid(tile.coordinate) || (board.get(tile.coordinate) != null)) {
                return false;
            }
        }
            return true;
    }


    @Override
    public void newGame() {
        random = new Random();
        int randomNumberRows = random.nextInt(this.rows);
        int randomNumberCols = random.nextInt(this.cols);
        Coordinates randCoord = new Coordinates(randomNumberRows, randomNumberCols);

        this.board = new Board(rows, cols);
        this.gamestate = GameStates.WELCOME_SCREEN;
        this.directions = Directions.NORTH;
    
        this.characterHead = new CharacterWithCoordinates(Character.Sneil, this.coord);
        this.point = new pointWithCoordinates(Character.Flower, randCoord);
        this.flowersEaten = 0;

        double time = Math.max(200*Math.pow(0.9+0.01,this.flowersEaten),70);
        getDelay(time);

     
    }

    @Override
    public void makesPoints() {            

        if (characterHead.getCoordinate().equals(point.getCoordinate())) {

            random = new Random();
            int randomNumberRows = random.nextInt(this.rows);
            int randomNumberCols = random.nextInt(this.cols);
            Coordinates randCoord = new Coordinates(randomNumberRows, randomNumberCols);

            this.point = new pointWithCoordinates(Character.Flower, randCoord);
            this.flowersEaten++;
            this.characterHead.eat();
        } 
    }

    @Override
    public int getRows() {
        return this.board.getRows();
    }

    @Override
    public int getCols() {
        return this.board.getCols();
    }

    @Override
    public int getScore() {
        return this.flowersEaten;
    }

    @Override
    public int getDelay(double time) {
        int initialTime = 200;
        time = Math.max(initialTime*Math.pow(0.9+0.01,this.flowersEaten),70);
        return (int)(time);
    }


    @Override
    public Iterable<CoordinatesThings<Tile>> tilesOnBoard() {
        return this.board;
    }

    @Override
    public Iterable<CoordinatesThings<Tile>> characterOnBoard() {
        return this.characterHead;

    }

    @Override
    public Iterable<CoordinatesThings<Tile>> pointsOnBoard() {
        return this.point;
    }

    @Override
    public Directions getDirection() {
        return this.directions;
    }

    @Override
    public Directions setDirection(Directions newDirection) {
        this.directions = newDirection;
        return directions;
    }

    @Override
    public GameStates getGameState() {
        return this.gamestate;
    }

    @Override
    public GameStates setGameState(GameStates newgamestate) {
        this.gamestate = newgamestate;
        return gamestate;
    }

    @Override
    public void moveCharacter(int row, int col) {
         characterHead.moveCharacter(row, col);

         if (!outOfBounds(characterHead) ||  characterHead.isDead() ) {


             setGameState(GameStates.GAMEOVER_SCREEN);
         }

    }   
    }
