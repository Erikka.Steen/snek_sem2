package inf101.sem2.game.model.structure;


import inf101.sem2.grid.Coordinates;
import inf101.sem2.grid.Grid;

/**Where the board is created, extended by the Grid that iterates the Tile to make the desired dimensions */
public class Board extends Grid<Tile>  {

    /**
     * The instructor of Board takes in rows and cols to be able to make dimensions through Grid<Tile>.
     * @param rows
     * @param cols
     */
    public Board(int rows, int cols) {
        super(rows, cols);

    }


    /**
     * A 2d array for testing. Hentet inspirasjon fra første semesteroppgave.
     * @return
     */
    public char[][] toCharArray2d() {
        char[][] result = new char[this.getRows()][this.getCols()];
        
        for (int row = 0; row < this.getRows(); row++) {
            for (int col = 0; col < this.getCols(); col++) {
                Tile tile = this.get(new Coordinates(row, col));
                char myCharacter = '-';
                if (tile != null) {
                    myCharacter = tile.symbol;
                }
                result[row][col] = myCharacter;
            }
            
        } 
        return result;
    }

}
