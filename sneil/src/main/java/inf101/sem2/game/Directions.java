package inf101.sem2.game;

/** The enum of the directions of the player. */
public enum Directions {
    NORTH,
    SOUTH,
    WEST,
    EAST
}
