package inf101.sem2.game;

/** The enum that decides the games gamestates. */
public enum GameStates {
    ACTIVE_GAME,
    PAUSED_GAME,
    WELCOME_SCREEN,
    GAMEOVER_SCREEN
}
