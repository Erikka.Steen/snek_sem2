package inf101.sem2.game.controller;
import inf101.sem2.game.Directions;
import inf101.sem2.game.GameStates;
import inf101.sem2.game.model.structure.characters.CharacterWithCoordinates;

/** Interface for the structure of the game */
public interface IGame {
    
/** With the given input, changes the direction of the character.
 * @param row
 * @param col */
void moveCharacter(int row, int col);

/**
 * Checks if the character is out of bounds.
 * @return true if the character is on the board, false if it's not on the board
 */
public boolean outOfBounds(CharacterWithCoordinates character);

/** When the game is game over, player can choose to restart the game. This method is then called to reset all the stats. */
public void newGame();

/**  @return which gamestate the game is currently in. */
GameStates getGameState();

/** Lets you change the gamestate.
 * @return a new gamestate */
GameStates setGameState(GameStates newgamestates);

/**  @return which direction the character is headed. */
Directions getDirection();

/** Sets a new direction with the given input.
 * @param directions
 * @return a new direction. */
Directions setDirection(Directions directions);

/** Uses the extension Timer.swing to determine a delay and time on the character's movements.
 * @return */ 
int getDelay(double time);

/** Counts how many times the character has the same coordinate as the point(flower) and relocates the point(flower) */
public void makesPoints();

}
