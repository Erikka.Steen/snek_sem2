package inf101.sem2.game.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.Timer;

import inf101.sem2.game.Directions;
import inf101.sem2.game.GameStates;
import inf101.sem2.game.model.structure.GameModel;
import inf101.sem2.game.model.structure.characters.pointWithCoordinates;
import inf101.sem2.game.model.visuals.GameVisual;


/** This is the class controlling the keybinds of the game. */
public class Controller implements KeyListener, ActionListener {
    

    GameVisual visual;
    IGame game;
    Timer timer;
    
    pointWithCoordinates characterCoordinates;
    GameModel gameModel;
    GameStates gameStates;
    Directions directions;

    int initialTime=200;
    double time;
    double newtime;


    /**
     * The constructor takes in two inputs, one of them being the structure of the game(GameModel), and the other being the visuals of the game.(GameVisual)
     * @param modelGame
     * @param visual
     */
    public Controller(GameModel modelGame, GameVisual visual){
        

        this.game = modelGame;
        this.visual = visual;
        visual.addKeyListener(this); 
        
        this.time = Math.max(initialTime*Math.pow(0.9+0.01,modelGame.getScore()),70);
        timer = new Timer(modelGame.getDelay(time), this);
        timer.start();

        this.newtime = Math.max(initialTime*Math.pow(0.9+0.01,modelGame.getScore()),70);

        System.out.print(timer);
    }


    /** Sets a timer for when the tiles move. */
    public void timerDelay() {
    int delay = game.getDelay(this.time);
    timer.setDelay(delay);
    timer.setInitialDelay(delay);

    }


    @Override
    public void keyPressed(KeyEvent e) {

        if ((e.getKeyCode() == KeyEvent.VK_SPACE) && (game.getGameState() == GameStates.WELCOME_SCREEN)) {
            game.setGameState(GameStates.ACTIVE_GAME);
            game.setDirection(Directions.NORTH);
            return;
        }

        else if ((e.getKeyCode() == KeyEvent.VK_LEFT) && (game.getGameState() == GameStates.ACTIVE_GAME) && (game.getDirection() != (Directions.EAST))) {
            game.setDirection(Directions.WEST);
            visual.repaint();
            timerDelay();
        }

        else if ((e.getKeyCode() == KeyEvent.VK_RIGHT) && (game.getGameState() == GameStates.ACTIVE_GAME) && (game.getDirection() != (Directions.WEST))) {
            game.setDirection(Directions.EAST);
            visual.repaint();
            timerDelay();
        }

        else if ((e.getKeyCode() == KeyEvent.VK_UP) && (game.getGameState() == GameStates.ACTIVE_GAME) && (game.getDirection() != (Directions.SOUTH))) {
            game.setDirection(Directions.NORTH);
            visual.repaint();
            timerDelay();
        }

        else if ((e.getKeyCode() == KeyEvent.VK_DOWN) && (game.getGameState() == GameStates.ACTIVE_GAME) && (game.getDirection() != (Directions.NORTH))) {
            game.setDirection(Directions.SOUTH);
            visual.repaint();
            timerDelay();
        }

        else if ((e.getKeyCode() == KeyEvent.VK_ESCAPE) && (game.getGameState() == GameStates.ACTIVE_GAME)) {
            game.setGameState(GameStates.PAUSED_GAME);
        }
        
        else if ((e.getKeyCode() == KeyEvent.VK_ESCAPE) && (game.getGameState() == GameStates.PAUSED_GAME)) {
            game.setGameState(GameStates.ACTIVE_GAME);
        }

        else if ((e.getKeyCode() == KeyEvent.VK_SPACE) && (game.getGameState() == GameStates.GAMEOVER_SCREEN)) {
            timerDelay();
            game.newGame();
        }

        visual.repaint();
    }


    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ((game.getGameState() == GameStates.ACTIVE_GAME) && (game.getDirection() == Directions.NORTH)) {
            game.moveCharacter(-1, 0);
            game.makesPoints();
            visual.repaint();
            timerDelay();
        }

        else if ((game.getGameState() == GameStates.ACTIVE_GAME) && (game.getDirection() == Directions.WEST)) {
            this.game.moveCharacter(0, -1);
            game.makesPoints();
            visual.repaint();
            timerDelay();
        }

        else if ((game.getGameState() == GameStates.ACTIVE_GAME) && (game.getDirection() == Directions.EAST)) {
            this.game.moveCharacter(0, 1);
            game.makesPoints();
            visual.repaint();
            timerDelay();
        }

        else if ((game.getGameState() == GameStates.ACTIVE_GAME) && (game.getDirection() == Directions.NORTH)) {
            this.game.moveCharacter(-1, 0);
            game.makesPoints();
            visual.repaint();
            timerDelay();
        }

        else if ((game.getGameState() == GameStates.ACTIVE_GAME) && (game.getDirection() == Directions.SOUTH)) {
            this.game.moveCharacter(1, 0);
            game.makesPoints();
            visual.repaint();
            timerDelay();
        }
    }
}
