package inf101.sem2.grid;

import java.util.Objects;

/** Koden CoordinatesThing er hentet fra Semesteroppgave 1 */

public class CoordinatesThings<T> {

    public final Coordinates coordinate;
    public final T thing;
    public Object item;

     /**
      * The constructor of CoordinateItem. Uses a coordinate and item.
      * @param coordinate
      * @param item
      */
    public CoordinatesThings(Coordinates coordinate, T item) {
        this.coordinate = coordinate;
        this.thing = item;
    }

    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CoordinatesThings<?> that = (CoordinatesThings<?>) o;
        return Objects.equals(coordinate, that.coordinate) && Objects.equals(thing, that.thing);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinate, thing);
    }

    @Override
    public String toString() {
        return "{ coordinate='{ row='" + coordinate.row + "', col='" + coordinate.col + "' }', item='" + thing.toString() + "' }";
    }
}
