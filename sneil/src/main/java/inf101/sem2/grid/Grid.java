package inf101.sem2.grid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/** Koden Grid er hentet fra Semesteroppgave 1 */

/** The class Grid takes in the type T, and implements the interface IGrid. */
public class Grid<T> implements IGrid<T> {

    private int rows;
    private int cols;
    private List<T> cells;

    
    /**
     * 
     *  Construct a grid with the given parameters.
     * @param row
     * @param column
     * @param defaultValue
     */
    Grid(int row, int column, T defaultValue) {
        if(row <= 0 || column <= 0) {
            throw new IllegalArgumentException();
        }

        this.rows = row;
        this.cols = column;
        this.cells = new ArrayList<>(cols * rows);
        for(int i = 0; i < cols * rows; ++i) {
            cells.add(defaultValue);
        }
    }

    /***
     * This is the grid's constructor.
     * @param row
     * @param column
     */
    public Grid(int row, int column) {
        this(row, column, null);
    }

    @Override
    public int getRows() {
        return rows;
    }

    @Override
    public int getCols() {
        return cols;
    }

    @Override
    public void set(Coordinates coordinate, T value) {
        checkCoordinate(coordinate);

        cells.set(coordinateToIndex(coordinate), value);
    }

    @Override
    public T get(Coordinates coordinate) {
        checkCoordinate(coordinate);

        return cells.get(coordinateToIndex(coordinate));
    }

    /**
     * Checks if the given coordinate is within bounds of the grid
     * @param coordinate the coordinate to check
     */
    private void checkCoordinate(Coordinates coordinate) {
        if(!CheckIfCoordinateIsOnGrid(coordinate)) {
            throw new IndexOutOfBoundsException("Row and column indices must be within bounds");
        }
    }

    /**
     * This method computes which index in the list belongs to a given Coordinate
     */
    private int coordinateToIndex(Coordinates coordinate) {
        return coordinate.row + coordinate.col * rows;
    }

    @Override
    public boolean CheckIfCoordinateIsOnGrid(Coordinates coordinate) {
        if(coordinate.row < 0 || coordinate.row >= rows) {
            return false;
        }
        return coordinate.col >= 0 && coordinate.col < cols;
    }

    @Override
    public Iterator<CoordinatesThings<T>> iterator() {
        List<CoordinatesThings<T>> coordinateItems = new ArrayList<>();

        for (int row = 0; row < getRows(); row++) {
            for (int col = 0; col < getCols(); col++) {
                Coordinates coordinate = new Coordinates(row, col);
                CoordinatesThings<T> coordinateItem = new CoordinatesThings<>(coordinate, get(coordinate));
                coordinateItems.add(coordinateItem);
            }
        }
        return coordinateItems.iterator();
    }
}

