package inf101.sem2.grid;


/** Koden IGrid er hentet fra Semesteroppgave 1 */

/** Store deler av koden er inspirert av seminarleder under semesteroppgave 1 */
/** The interface of type T that implements Grid, and extends iterable. */
public interface IGrid<T> extends Iterable<CoordinatesThings<T>> {


    /** @return the number of rows in the grid */
    int getRows();

    /** @return the number of columns in the grid */
    int getCols();

    /**
     * Sets the value of a postion in the grid. A subsequent call to {@link #get}
     * with an equal coordinate as argument will return the value which was set. The
     * method will overwrite any previous value that was stored at the location.
     * 
     * @param coordinate the location in which to store the value
     * @param value      the new value
     * @throws IndexOutOfBoundsException if the coordinate is not within bounds of
     *                                   the grid
     */
    void set(Coordinates coordinate, T value);

    /**
     * Gets the current value at the given coordinate.
     * 
     * @param coordinate the location to get
     * @return the value stored at the coordinate
     * @throws IndexOutOfBoundsException if the coordinate is not within bounds of
     *                                   the grid
     */
    T get(Coordinates coordinate);

    /**
     * Tells you if the coordinate is within bounds for this grid
     * 
     * @param coordinate coordinate to check
     * @return true if the coordinate is within bounds, false otherwise
     */
    boolean CheckIfCoordinateIsOnGrid(Coordinates coordinate);
}
