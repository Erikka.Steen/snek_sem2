package inf101.sem2.grid;

import java.util.Objects;

/** Koden Coordinates er hentet fra Semesteroppgave 1 */

public class Coordinates {

    public final int row;
    public final int col;
    public Object coord;

    /**
     * The constructor of Coordinate that makes the coordinate of row and col.
     * @param row
     * @param col
     */
    public Coordinates(int row, int col) {
        this.row = row;
        this.col = col;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinates that = (Coordinates) o;
        return row == that.row && col == that.col;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, col);
    }

    @Override
    public String toString() {
        return "{ row='" + row + "', col='" + col + "' }";
    }


    /**
     * The method takes input of row and col and changes them.
     * @param deltarow
     * @param deltacol
     * @return new row and col, depending on input.
     */
    public Coordinates changeCoordinates(int deltarow, int deltacol) {
        
    int newrow = row + deltarow;
    int newcol = col + deltacol;

    return new Coordinates(newrow, newcol);
    }
}

