package inf101.sem2.grid;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

/**Testene er hentet fra Seminaroppgave 1
 * Testing the class Grid.
 * Store deler at testene er inspirert av seminarleder.
 */
public class GridTest {

    @Test
    void gridTestGetRowsAndCols() {
        IGrid<Integer> testGrid = new Grid<>(3, 2);
        assertEquals(3, testGrid.getRows());
        assertEquals(2, testGrid.getCols());
    }

    @Test
    void gridSanityTest() {
        String defaultValue = "x";
        IGrid<String> testGrid = new Grid<>(3, 2, defaultValue);

        assertEquals(3, testGrid.getRows());
        assertEquals(2, testGrid.getCols());

        assertEquals("x", testGrid.get(new Coordinates(0, 0)));
        assertEquals("x", testGrid.get(new Coordinates(2, 1)));

        testGrid.set(new Coordinates(1, 1), "y");

        assertEquals("y", testGrid.get(new Coordinates(1, 1)));
        assertEquals("x", testGrid.get(new Coordinates(0, 1)));
        assertEquals("x", testGrid.get(new Coordinates(1, 0)));
        assertEquals("x", testGrid.get(new Coordinates(2, 1)));
    }

    @Test
    void gridCanHoldNull() {
        String defaultValue = "x";
        IGrid<String> testGrid = new Grid<>(3, 2, defaultValue);

        assertEquals("x", testGrid.get(new Coordinates(0, 0)));
        assertEquals("x", testGrid.get(new Coordinates(2, 1)));

        testGrid.set(new Coordinates(1, 1), null);

        assertEquals(null, testGrid.get(new Coordinates(1, 1)));
        assertEquals("x", testGrid.get(new Coordinates(0, 1)));
        assertEquals("x", testGrid.get(new Coordinates(1, 0)));
        assertEquals("x", testGrid.get(new Coordinates(2, 1)));
    }

    @Test
    void gridNullsInDefaultConstructor() {
        IGrid<String> testGrid = new Grid<>(3, 2);

        assertEquals(null, testGrid.get(new Coordinates(0, 0)));
        assertEquals(null, testGrid.get(new Coordinates(2, 1)));

        testGrid.set(new Coordinates(1, 1), "y");

        assertEquals("y", testGrid.get(new Coordinates(1, 1)));
        assertEquals(null, testGrid.get(new Coordinates(0, 1)));
        assertEquals(null, testGrid.get(new Coordinates(1, 0)));
        assertEquals(null, testGrid.get(new Coordinates(2, 1)));
    }

    @Test
    void coordinateIsOnGridTest() {
        IGrid<Double> testGrid = new Grid<>(3, 2, 0.9);

        assertTrue(testGrid.CheckIfCoordinateIsOnGrid(new Coordinates(2, 1)));
        assertFalse(testGrid.CheckIfCoordinateIsOnGrid(new Coordinates(3, 1)));
        assertFalse(testGrid.CheckIfCoordinateIsOnGrid(new Coordinates(2, 2)));

        assertTrue(testGrid.CheckIfCoordinateIsOnGrid(new Coordinates(0, 0)));
        assertFalse(testGrid.CheckIfCoordinateIsOnGrid(new Coordinates(-1, 0)));
        assertFalse(testGrid.CheckIfCoordinateIsOnGrid(new Coordinates(0, -1)));
    }

    @Test
    void throwsExceptionWheenCoordinateOffGrid() {
        IGrid<String> testGrid = new Grid<>(3, 2, "x");

        try {
            @SuppressWarnings("unused")
            String x = testGrid.get(new Coordinates(3, 1));
            fail();
        } catch (IndexOutOfBoundsException e) {
            // Test passed
        }
    }

    @Test
    void testIterator() {
        IGrid<String> testGrid = new Grid<>(3, 2, "x");
        testGrid.set(new Coordinates(0, 0), "a");
        testGrid.set(new Coordinates(1, 1), "b");
        testGrid.set(new Coordinates(2, 1), "c");

        List<CoordinatesThings<String>> items = new ArrayList<>();
        for (CoordinatesThings<String> coordinateItem : testGrid) {
            items.add(coordinateItem);
        }

        assertEquals(3 * 2, items.size());
        assertTrue(items.contains(new CoordinatesThings<String>(new Coordinates(0, 0), "a")));
        assertTrue(items.contains(new CoordinatesThings<String>(new Coordinates(1, 1), "b")));
        assertTrue(items.contains(new CoordinatesThings<String>(new Coordinates(2, 1), "c")));
        assertTrue(items.contains(new CoordinatesThings<String>(new Coordinates(0, 1), "x")));
    }
    
}
