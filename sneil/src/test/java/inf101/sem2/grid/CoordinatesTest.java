package inf101.sem2.grid;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Objects;

import org.junit.jupiter.api.Test;

/**Testene er hentet fra Seminaroppgave 1
 * Testing the class Coordinate
 * Store deler av testene er inspirert av seminarleder.
 */
public class CoordinatesTest {

    @Test
    void coordinateSanityTest() {
        Coordinates c = new Coordinates(4, 3);
        assertEquals(4, c.row);
        assertEquals(3, c.col);
    }

    @Test
    void coordinateToStringTest() {
        Coordinates c1 = new Coordinates(4, 3);
        assertEquals("{ row='4', col='3' }", c1.toString());

        Coordinates c2 = new Coordinates(6, 1);
        assertEquals("{ row='6', col='1' }", c2.toString());
    }

    @Test
    void coordinateEqualityTest() {
        Coordinates a = new Coordinates(2, 3);
        Coordinates b = new Coordinates(2, 3);

        assertFalse(a == b);
        assertTrue(a.equals(b));
        assertTrue(b.equals(a));
        assertTrue(Objects.equals(a, b));
    }

    @Test
    void coordinateInequalityTest() {
        Coordinates a = new Coordinates(2, 3);
        Coordinates b = new Coordinates(3, 2);

        assertFalse(a == b);
        assertFalse(a.equals(b));
        assertFalse(b.equals(a));
        assertFalse(Objects.equals(a, b));
    }

    @Test
    void coordinateHashcodeTest() {
        Coordinates a = new Coordinates(2, 3);
        Coordinates b = new Coordinates(2, 3);
        assertTrue(a.hashCode() == b.hashCode());

        Coordinates c = new Coordinates(100, 100);
        Coordinates d = new Coordinates(100, 100);
        assertTrue(c.hashCode() == d.hashCode());
    }
}
