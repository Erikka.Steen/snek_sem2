package inf101.sem2.grid;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Objects;
import org.junit.jupiter.api.Test;

/**Testene er hentet fra Seminaroppgave 1
 * Testing the class CoordinateItem.
 * Store deler av testene er inspirert av seminarleder.
 */
public class CoordinatesThingsTest {

    
    @Test
    void coordinateItemSanityTest() {
        String item = "Test";
        Coordinates coordinate = new Coordinates(4, 2);
        CoordinatesThings<String> coordinateItem = new CoordinatesThings<>(coordinate, item);

        assertEquals(coordinate, coordinateItem.coordinate);
        //assertEquals(item, coordinateItem.item);
    }

    @Test
    void coordinateItemToStringTest() {
        String item = "Test";
        Coordinates coordinate = new Coordinates(4, 2);
        CoordinatesThings<String> coordinateItem = new CoordinatesThings<>(coordinate, item);

        assertEquals("{ coordinate='{ row='4', col='2' }', item='Test' }", coordinateItem.toString());
    }

    @Test
    void coordinateItemEqualityAndHashCodeTest() {
        String item = "Test";
        Coordinates coordinate = new Coordinates(4, 2);
        CoordinatesThings<String> coordinateItem = new CoordinatesThings<>(coordinate, item);

        String item2 = "Test";
        Coordinates coordinate2 = new Coordinates(4, 2);
        CoordinatesThings<String> coordinateItem2 = new CoordinatesThings<>(coordinate2, item2);

        assertTrue(coordinateItem2.equals(coordinateItem));
        assertTrue(coordinateItem.equals(coordinateItem2));
        assertTrue(Objects.equals(coordinateItem, coordinateItem2));
        assertTrue(coordinateItem.hashCode() == coordinateItem2.hashCode());
    }

    @Test
    void coordinateItemInequalityTest() {
        String item = "Test";
        Coordinates coordinate = new Coordinates(4, 2);
        CoordinatesThings<String> coordinateItem = new CoordinatesThings<>(coordinate, item);

        String item2 = "Test2";
        Coordinates coordinate2 = new Coordinates(2, 4);

        CoordinatesThings<String> coordinateItem2 = new CoordinatesThings<>(coordinate2, item);
        CoordinatesThings<String> coordinateItem3 = new CoordinatesThings<>(coordinate, item2);

        assertFalse(coordinateItem2.equals(coordinateItem));
        assertFalse(coordinateItem.equals(coordinateItem2));
        assertFalse(coordinateItem.equals(coordinateItem3));
        assertFalse(coordinateItem2.equals(coordinateItem3));
        assertFalse(Objects.equals(coordinateItem, coordinateItem2));
        assertFalse(Objects.equals(coordinateItem, coordinateItem3));
        assertFalse(Objects.equals(coordinateItem2, coordinateItem3));
    }
    
}
